img#logo {
    border-radius: 15px;
    width: 30px;
    height: 30px;
    margin-right: 0.5em;
}

small.smaller {
    font-size: 0.75em;
}

h1 {
    margin-top: 3rem;
}

h2 {
    margin-top: 1.75rem;
    margin-bottom: 1em;
}

h3 {
    margin-top: 1.25rem;
}

h4 {
    margin-top: 1rem;
}

.nav-section {
    margin-top: 2em;
}

.monospaced {
    font-family: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
}

a.sidebar-nav-pill,
a.sidebar-nav-pill:active,
a.sidebar-nav-pill:hover {
    color: #444;
}

.module-source > details > pre {
    display: block;
    overflow-x: auto;
    overflow-y: auto;
    max-height: 600px;
    font-size: 0.8em;
}

a {
    color: #e83e8c;
}

.breadcrumb-item.inactive > a {
    color: #a626a4 !important;
}

.breadcrumb-item.active > a {
    color: #e83e8c !important;
}

.breadcrumb-item+.breadcrumb-item::before {
    content: ".";
}

.module-breadcrumb {
    padding-left: 0 !important;
}

ul.nested {
    margin-left: 1em;
}

h2#parameters::after {
    margin-left: 2em;
}

dt {
    margin-left: 2em;
}

dd {
    margin-left: 4em;
}

dl.no-nest > dt {
    margin-left: 0em;
}

dl.no-nest > dd {
    margin-left: 2em;
}

dl.root {
    margin-bottom: 2em;
}

.definition {
    display: block;
    margin-bottom: 8em !important;
}

.definition .row {
    display: block;
    margin-bottom: 4em !important;
}

.definition h2 {
    font-size: 1em;
    font-weight: bolder;
}

.sep {
    height: 2em;
}

code {
    color: #a626a4;
}

code .active {
    color: #e83e8c;
}

code a {
    color: #e83e8c;
}

a.dotted {
    text-decoration: underline #6c757d dotted !important;
}

## Custom search formatting to look somewhat bootstrap-py
.gsc-search-box, .gsc-search-box-tools, .gsc-control-cse {
    background: none !important;
    border: none !important;
}

.gsc-search-button-v2, .gsc-search-button-v2:hover, .gsc-search-button-v2:focus {
    color: var(--success) !important;
    border-color: var(--success) !important;
    background: none !important;
    padding: 6px 32px !important;
    font-size: inherit !important;
}

.gsc-search-button-v2 > svg {
    fill: var(--success) !important;
}

.gsc-input-box {
    border-radius: 3px;
}

.gsc-control-cse {
    width: 300px !important;
    margin-top: 0 !important;
}

.gsc-control-cse .gsc-control-cse-en {
    margin-top: 0 !important;
}
