# Copyright © Nekoka.tt 2019-2020
#
# This file is part of Hikari.
#
# Hikari is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Hikari is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Hikari. If not, see <https://www.gnu.org/licenses/>.

###
### Base for making a release to PyPI.
###
.release:
  allow_failure: false
  before_script:
    - >
      git log -n 1 "${CI_COMMIT_SHA}" --format="%B" \
          | grep -iqE "\[\s*(skip|no|don'?t|do\s+not)\s+deploy(ments?)?\s*\]" \
        && echo "SKIPPING ${CI_JOB_STAGE} STAGE JOB" \
        && exit 0
    # We don't start on the branch, but a ref of it, which isn't useful
    - echo "Changing from ref to actual branch"
    - git fetch -ap
    - git checkout ${TARGET_BRANCH}
    - git reset --hard origin/${TARGET_BRANCH}
    - >
      echo
      echo "========================= hikari/_about.py ========================="
      echo
      cat -n hikari/_about.py
      echo
      echo "===================================================================="
      echo
    - pip install twine wheel nox
  extends: .cpython
  interruptible: false
  resource_group: deploy
  retry: 1
  stage: deploy

###
### Make a dev release.
###
### Upload the current code to PyPI as a .dev release.
### This will only work on the staging branch.
###
release:staging:
  environment:
    name: staging
    url: https://nekokatt.gitlab.io/hikari
  extends: .release
  script:
    - nox -s deploy --no-error-on-external-run
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME == 'staging'"
  variables:
    TARGET_BRANCH: staging

###
### Make a production release.
###
### Upload the current code to PyPI as a production release. This will only
### work on the master branch.
###
release:master:
  environment:
    name: prod
    url: https://nekokatt.gitlab.io/hikari
  extends: .release
  script:
     - apt-get update
     - apt-get install curl openssh-client -qy

     # Don't dox our keys, or spam echos
     - set +x

     # Init the SSH agent and add our gitlab private key to it.
     - echo ">>>> Starting SSH agent <<<<"
     - eval "$(ssh-agent -s)"
     - echo ">>>> Making ~/.ssh <<<<"
     - mkdir ~/.ssh || true
     - echo ">>>> Writing SSH private key to ~/.ssh/id_rsa <<<<"
     - echo "${GIT_SSH_PRIVATE_KEY}" > ~/.ssh/id_rsa
     - echo ">>>> SSH IS READY <<<<"

     # Be verbose again.
     - set -x

     - chmod 600 ~/.ssh/id_rsa
     - ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
     - ssh-add ~/.ssh/id_rsa

     - nox -s deploy --no-error-on-external-run
  rules:
    - if: "$CI_PIPELINE_SOURCE == 'push' && $CI_COMMIT_TAG == null && $CI_COMMIT_REF_NAME == 'master'"    
  variables:
    TARGET_BRANCH: master
